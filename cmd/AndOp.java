class AndOp //AND operator
{
	public static void main(String[]args)
	{
		int num1=10;
		int num2=20;
		int num3=30;

		System.out.println("main starts.");

		boolean result=(num1<num2)&&(num3>num2); //true true
		System.out.println(result);

		boolean result1=(num1>num2)&&(num2>num3); //false false
		System.out.println(result1);

		boolean result2=(num1<num2)&&(num2>num3); //true false
		System.out.println(result2);

		boolean result3=(num1>num3)&&(num2<num3); //false true
		System.out.println(result3);

		System.out.println("main ends.");
	}
}

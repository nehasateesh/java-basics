class Mark
{
	public static void main(String[]args)
	{
		int mark=60;

		if(mark>=81 && mark<=100)
		{
			System.out.println("Distinction.");
		}
		else if(mark>=35 && mark<=80)
		{
			System.out.println("First class.");
		}
		else if(mark>=0 && mark<=34)
		{
			System.out.println("Oops, failed. :(");
		}
		else
		{
			System.out.println("INVALID MARKS.");
		}
	}
}
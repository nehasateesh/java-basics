class Gt
{
	public static void main(String[]args)
	{
		System.out.println("main() starts");

		int num1=50;
		int num2=100;

		String res=num1<num2?"num1 is the greatest number":"num2 is the greatest number";

		System.out.println(res);

		System.out.println("main() ends");
	}
}

public class From
{
	public static void main(String[] args)
	{
		System.out.println("main() is starting");

		m3();
		m1();

		System.out.println("main() is ending");
	}
	
	public static void m1()
	{
		m2();
		System.out.println("m1() has been called");
	}

	public static void m2()
	{
		System.out.println("m2() has been called");
	}

	public static void m3()
	{
		System.out.println("m3() has been called");
	}

}

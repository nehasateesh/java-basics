class Switch
{
	public static void main(String[]args)
	{
		int choice=1;

		switch(choice)
		{
			case 1:
			{
				System.out.println("case1 is executing");
				break;
			}

			case 2:
			{
				System.out.println("case2 is executing");
				break;
			}
		
			case 3:
			{
				System.out.println("case1 is executing");
				break;
			}

			default:
			{
				System.out.println("INVALID CHOICE!!");
				break;
			}
		}
	}
}


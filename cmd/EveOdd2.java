class EveOdd2
{
	public static void main(String[]args)
	{
		System.out.println("main() starts");

		int num1=2;

		String res=num1%2==0?num1+" is a even number":num1+" is the odd number"; //concatenation

		System.out.println(res);

		System.out.println("main() ends");
	}
}

import java.util.Scanner;
public class Scann1
{
	public static void main(String[] args)
	{
		System.out.println("main() starts");
		Scanner sc= new Scanner(System.in);

		System.out.println("Enter the byte value");
			byte b=sc.nextByte();
		System.out.println(b);

		System.out.println("Enter your name");
			int i=sc.nextInt();
		System.out.println(i);

		System.out.println("Enter the float value");
			float f=sc.nextFloat();
		System.out.println(f);

		System.out.println("Enter the char value");
			char ch=sc.next().charAt(2);
		System.out.println(ch);

		System.out.println("Enter the string value");
			String s=sc.next();
		System.out.println(s);

		System.out.println("main() ends");
	}
}
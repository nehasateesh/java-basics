public class Arth4
{
	public static void main(String[] args)
	{
		System.out.println("main starts");
		add(10,20);
		sub(500,200);
		multi(5,100);
		div(200,200);
		System.out.println("main ends");
	}

	public static void add(int a, int b)
	{
		System.out.println("addition starts");
		int i=a+b;
		System.out.println(i);
		System.out.println("addition ends");
	}


	public static void sub(int a, int b)
	{
		System.out.println("Subtraction starts");
		int i=a-b;
		System.out.println(i);
		System.out.println("Subtraction ends");
	}

	public static void multi(int a, int b)
	{
		System.out.println("Multiplication starts");
		int i=a*b;
		System.out.println(i);
		System.out.println("Multiplication ends");
	}

	public static void div(int a, int b)
	{
		System.out.println("Division starts");
		int i=a/b;
		System.out.println(i);
		System.out.println("Divison ends");
	}
}

class Not
{
	public static void main(String[]args)
	{
		int a=10;
		int b=20;
		int c=30;

		boolean res=!(a<b)||(c>b);//true
		System.out.println(res);

		boolean res2=!(a>b)||(b>c);
		System.out.println(res2);

		boolean res3=!(a<b)||(b>c);
		System.out.println(res3);

		boolean res4=!(a>b)||(b<c);
		System.out.println(res4);
	}
}
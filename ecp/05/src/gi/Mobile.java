package gi;

abstract public class Mobile 
{
	abstract public void  ram();
	abstract public void  rom();
	abstract public void  sim();
	abstract public void  apps();
	abstract public void  display();
}

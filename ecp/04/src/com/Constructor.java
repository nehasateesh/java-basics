package com;

public class Constructor 
{
	long price;
	String s;
	float f;
	
	public static void main(String[] args) 
	{
		Constructor c=new Constructor();
		Constructor c1=new Constructor(10);
		Constructor c2=new Constructor(10);
		Constructor c3=new Constructor(20,30.0f);
		Constructor c4=new Constructor(20,30.0f, null);
	}
	Constructor()
	{
		
	}
	Constructor(int a)
	{
		this.price=a;
	}
	Constructor(int a, float fi)
	{
		this.price=a;
		float f=fi;
	}
	Constructor(int a, float f, String s)
	{
		this.price=a;
		this.s=s;
		this.f=f;
	}
}

package com;

public class CastDrive extends Ola
{

	public static void main(String[] args) 
	{
		//UpCasting
		Ola o=new Cab();
		System.out.println(o.type);
		System.out.println(o.comfy);
		o.olaDetails();
		
		//Downcasting
		Cab c=(Cab)o;
		System.out.println(c.price);
		System.out.println(c.cabDriver);
		c.cabDetails();
		System.out.println(c.type);
		System.out.println(c.comfy);
		c.olaDetails();
		
	}

}

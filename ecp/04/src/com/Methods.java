package com;

public class Methods //method overloading
{
	public void m1(int a, String b)
	{System.out.println("m1 is executing");}
	
	public void m1(int a, int b, int c)
	{System.out.println("m2 is executing");}
	
	public void m1(String b, int a)
	{System.out.println("m3 is executing");}
	
	public void m1(float a, int b)
	{System.out.println("m4 is executing");}
	
	public void m1(int a, float b)
	{System.out.println("m5 is executing");}
	
	public static void main(String[] args) 
	{
		Methods mo=new Methods();
		mo.m1(30, 20.0f);
		mo.m1(9.0f, 22);
		mo.m1("hello", 65);
		mo.m1(1, 2, 3);
		mo.m1(89, "bye");
		
	}
}

package com;

public class Cast 
{
	public static void main(String[] args) 
	{
		//widening
		char ch='h';//104
		int i=ch;
		System.out.println(i);
		
		//narrowing
		int a=98;//b
		char ch1=(char)a;
		System.out.println(ch1);
		
		//add
		int i1='d'+'d';//100+100=200 means ASCII value of d is 100
		System.out.println(i1);
	}

}

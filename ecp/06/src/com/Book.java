package com;

public class Book 
{
	int bid;
	long bprice;
	Book(int a, long b1)
	{
		this.bid=a;
		this.bprice=b1;
	}
	@Override
	public String toString() //toString operator
	{
		return "bid="+bid+" bprice="+bprice;
	}
	@Override
	public boolean equals(Object o) //equals operator returns boolean value (true or false)
	{
		Book b2=(Book)o;
		return this.bid==b2.bid || this.bprice==b2.bprice;//can you or operator or and operator
	}
	@Override
	public int hashCode()//hashCode operator
	{
		return 12345;
	}
	public static void main(String[] args) 
	{
		Book b=new Book(76,987);
		Book b1=new Book(76, 98);
		
		System.out.println(b);
		System.out.println(b1);
		System.out.println(b==b1);//compares only address if not used equals method
		System.out.println(b.equals(b1));//compares address if equals not used
		System.out.println(b.hashCode());
		System.out.println(b1.hashCode());//if execute this without hashCode operator it returns the value of b and b1
	}
}

package array;

import java.util.*;

public class Arr3 
{
	public static void main(String[] args) 
	{
		ArrayList al=new ArrayList();
		al.add(10);
		al.add(20);
		al.add(30);
		LinkedList li=new LinkedList();
		li.add(40);
		li.add(50);
		al.addAll(li);
		System.out.println(al);
		System.out.println(al.contains(li));
		//System.out.println(li.remove(2));
		System.out.println(li.remove(0));
		System.out.println(li);
		System.out.println(al.containsAll(li));
		System.out.println(al.removeAll(li));
		System.out.println(al);
	}
}

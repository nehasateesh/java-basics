package array;

import java.util.ArrayList;

public class Arr 
{

	public static void main(String[] args) 
	{
		ArrayList al = new ArrayList();
		al.add(10);//add an element
		//al.add(3,98);//add element for particular position
		al.add(null);
		al.add("java");
		al.add("java");
		al.add(76);
		al.add(65);
		System.out.println(al);//toString() overridden
		System.out.println("-----------");
		System.out.println(al.size());//returns size
		System.out.println(al.get(1));//gets the value on particular position
		System.out.println(al.remove(3));//removes the value
		System.out.println(al.isEmpty());//check whether collection is
		al.clear();//removes an element from the collection
		System.out.println();
		

	}

}

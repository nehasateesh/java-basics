package array;

import java.util.ArrayList;

public class Arr2 
{
	public static void main(String[] args) 
	{
		ArrayList al=new ArrayList();
		al.add(10);
		al.add(20);
		al.add(30);
		al.add(40);
		al.add(10);
		al.add(20);
		al.add(30);
		al.add(89);
		al.add(10);
		System.out.println(al.indexOf(30));
		System.out.println(al.indexOf(89));
		System.out.println(al.lastIndexOf(30));//indexes from last
		
		System.out.println(al.indexOf(100));//returns -ve value cause not in the list
		System.out.println(al);
		ArrayList al1=new ArrayList();
		al1.add("pizza");
		al1.add("burger");
		al1.add("french fries");
		al1.add("sandwich");
		System.out.println(al1);
		System.out.println(al.add(al1));
		System.out.println(al);
		System.out.println(al1);
		System.out.println(al.addAll(al1));
		System.out.println(al);
		System.out.println(al1);
	}
}

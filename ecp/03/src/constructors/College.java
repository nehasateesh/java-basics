package constructors;

public class College 
{
		private String cname;
		private int noOfStudents;
		private boolean holidays;
		
		//getter
		public String getCname()
		{
			return cname;
		}
		
		public int getNoOfStudents()
		{
			return noOfStudents;
		}
		
		public boolean getHolidays()
		{
			return holidays;
		}
		
		//setter
		public void setcname(String a)
		{
			this.cname=a;
		}
		
		public void setNoOfStudents(int a)
		{
			this.noOfStudents=a;
		}
		
		public void setHolidays(boolean a)
		{
			this.holidays=a;
		}
		
	
}

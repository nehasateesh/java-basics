package com;

public class ShoeDriver 
{
	public static void main(String[] args) 
	{
		Shoe s= new Shoe();
		s.brand="puma";
		s.size=10;
		s.colour="black";
		s.ShoeDetails();
		System.out.println("__________");
		
		Shoe s1= new Shoe();
		s1.brand="woodland";
		s1.size=7;
		s1.colour="brown";
		s1.ShoeDetails();
		System.out.println("__________");
		
		Shoe s2= new Shoe();
		s2.brand="adidas";
		s2.size=8;
		s2.colour="white";
		s2.ShoeDetails();
		System.out.println("__________");
		
		Shoe s3= new Shoe();
		s3.brand="rebook";
		s3.size=9;
		s3.colour="grey";
		s3.ShoeDetails();
		System.out.println("__________");
		
		Shoe s4= new Shoe();
		s4.brand="decathlon";
		s4.size=6;
		s4.colour="maroon";
		s4.ShoeDetails();
		System.out.println("__________");
		
		Shoe s5= new Shoe();
		s5.brand="bata";
		s5.size=4;
		s5.colour="pink";
		s5.ShoeDetails();
		System.out.println("__________");
		
	}
}

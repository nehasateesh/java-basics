package Methods;

public class hai3 
{
	static //initializer syntax
	{
		System.out.println("initializer1 is executing");
	}
	static
	{
		System.out.println("intializer2 is executing");
	}
	static String s;
	public static void main(String[] args) 
	{
		System.out.println("main starts");
		double d=33.09;
		System.out.println(s);
		System.out.println(d);
		System.out.println("main ends");
	}
	static 
	{
		System.out.println("initializer3 is executing");
	}
}

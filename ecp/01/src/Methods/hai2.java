package Methods;

public class hai2 
{
	public static void main(String[] args) 
	{
		person1();
		person2();
		person3();
	}
	
	public static void person1() 
	{
		System.out.println("person1 is executing");
	}
	
	public static void person2()
	{
		System.out.println("person2 is executing");
	}
	
	public static void person3() 
	{
		System.out.println("person3 is executing");
	}
}
